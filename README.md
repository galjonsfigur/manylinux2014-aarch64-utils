# Notes about building OpenCV an NumPy on ARM64 system

This repository contains some loose notes and patches I created while I was preparing Python Wheels for OpenCV and NumPy for Odroid-N2 Single Board Computer.

### Tips:
- use `GNU screen` or simmilar program to allow work even if you disconnect from SSH.
- save history every command: https://unix.stackexchange.com/questions/1288/preserve-bash-history-in-multiple-terminal-windows
- Resuming a container can be done via:

```
docker start  `docker ps -q -l` # restart it in the background
docker attach `docker ps -q -l` # reattach the terminal & stdin
```

- In Odroid-N2 case I couldn't get any USB HDD to work correctly (rare but critical data corruption was happening) so I worked on SD card - not the best or fastest solution but at least there was no strange filesystem data corruptions.

### Patches in this repository
- `numpy-gcc7-ice-bug.patch` - Addresses issue with Internal Compiler Error in GCC shipped with `anylinux2014_aarch64` Docker container. Source of patch: https://github.com/numpy/numpy/issues/13622
- `qt-4.8.7-gcc7build-patch.patch` - Allows to compile QT4 with GCC7 compiler. Source of patch: https://forum.qt.io/topic/75842/can-t-compile-qt-4-8-7/13

### How to build OpenCV and NumPy
Don't forget to use `screen` or similar!

Run `manylinux2014` container:

- ``docker pull quay.io/pypa/manylinux2014_aarch64`
- `docker run -it quay.io/pypa/manylinux2014_aarch64 /bin/bash `

Build NumPy (with OpenBLAS or ATLAS/w LINPACK):

```sh
cd /root/

# For build with ATLAS and netlib LAPACK
curl -O -L https://sourceforge.net/projects/math-atlas/files/Stable/3.10.3/atlas3.10.3.tar.bz2 && \
tar -xf atlas3.10.3.tar.bz2 && \
cd ATLAS && \
curl -O -L http://www.netlib.org/lapack/lapack-3.9.0.tgz && \
tar -xf lapack-3.9.0.tgz && \
cd lapack-3.9.0 && \
#compile LAPACK separately because of some unknown reason ATLAS forgets to link all LAPACK routines
mkdir lapack_build && \
cd lapack_build && \
cmake .. -DCMAKE_POSITION_INDEPENDENT_CODE=ON && \
make -j$(getconf _NPROCESSORS_ONLN) && \
cd ../../ && \
mkdir build && cd build && \
#use 1 core only for compiling!
../configure --with-netlib-lapack-tarfile=lapack-3.9.0.tgz --prefix=/opt/atlas && \
make build && \
#Add rest of LAPACK routines to the lib
mkdir tmp && cd tmp && \
ar -x ../lib/liblapack.a && \
cp ../../lapack-3.9.0/lapack_build/lib/liblapack.a ../lib/liblapack.a && \
ar r ../lib/liblapack.a *.o && \
cd .. &&
rm -rf tmp && \
make install


# For build with OpenBLAS
curl -O -L https://github.com/xianyi/OpenBLAS/archive/v0.3.9.tar.gz && \
tar -xf v0.3.9.tar.gz && \
cd OpenBLAS-0.3.9/ && \
make -j$(getconf _NPROCESSORS_ONLN) && \
make PREFIX=/opt/OpenBLAS install

#Prepare NumPy
#install Cython for python version you are using
/opt/python/cp36-cp36m/bin/pip install Cython
cd /root/ && \

curl -O -L https://github.com/numpy/numpy/releases/download/v1.18.2/numpy-1.18.2.tar.gz && \
tar -xf numpy-1.18.2.tar.gz && \
cd numpy-1.18.2 && \
curl -O -L https://gitlab.com/galjonsfigur/manylinux2014-aarch64-utils/-/raw/master/numpy-gcc7-ice-bug.patch && \
patch -p0 < numpy-gcc7-ice-bug.patch

# For build with OpenBLAS and netlib LAPACK
cat > site.cfg <<EOF
[openblas]
libraries = openblas
library_dirs = /opt/OpenBLAS/lib
include_dirs = /opt/OpenBLAS/include
EOF
export NPY_BLAS_ORDER="openblas" && \
export NPY_LAPACK_ORDER="openblas"


# For build with ATLAS
cat > site.cfg <<EOF
[atlas]
library_dirs = /opt/atlas/lib
include_dirs = /opt/atlas/include
EOF
export NPY_BLAS_ORDER="ATLAS" && \
export NPY_LAPACK_ORDER="ATLAS"

#build wheel:
/opt/python/cp36-cp36m/bin/python setup.py bdist_wheel && \
cd dist && \
# For OpenBLAS
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/opt/OpenBLAS/lib"
#OR For ATLAS
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/opt/atlas/lib"

auditwheel repair *.whl

#Here you go!

```


Build other dependencies and OpenCV itself (script bellow is just slightly modified `Dockerfile` taken from https://github.com/skvark/opencv-python/blob/master/docker/Dockerfile_x86_64)

```sh
#Compile QT for GUI

cd /root/

curl -O -L https://download.qt.io/archive/qt/4.8/4.8.7/qt-everywhere-opensource-src-4.8.7.tar.gz && \
    tar -xf qt-everywhere-opensource-src-4.8.7.tar.gz && \
    cd qt-everywhere* && \
    # Patch QT to build under GCC7
    curl -O -L https://gitlab.com/galjonsfigur/manylinux2014-aarch64-utils/-/raw/master/qt-4.8.7-gcc7build-patch.patch && \
    patch -p1 < qt-4.8.7-gcc7build-patch.patch && \
    #configure does a bootstrap make under the hood
    #manylinux1 is too old to have `nproc`
    export MAKEFLAGS=-j$(getconf _NPROCESSORS_ONLN) && \
    #OpenCV only links against QtCore, QtGui, QtTest
    ./configure -prefix /opt/Qt4.8.7 -release -opensource -confirm-license \
    -no-sql-sqlite -no-qt3support -no-xmlpatterns -no-multimedia \
    -no-webkit -no-script -no-declarative -no-dbus -make libs && \
    make && \
    make install && \
    cd .. && \
    rm -rf qt-everywhere-opensource-src-4.8.7 && \
    rm qt-everywhere-opensource-src-4.8.7.tar.gz

export QTDIR="/opt/Qt4.8.7"
export PATH="$QTDIR/bin:$PATH"

# Build newer CMake
curl -O -L https://cmake.org/files/v3.9/cmake-3.9.0.tar.gz && \
    tar -xf cmake-3.9.0.tar.gz && \
    cd cmake-3.9.0 && \
    #manylinux1 provides curl-devel equivalent and libcurl statically linked
    # against the same newer OpenSSL as other source-built tools
    # (1.0.2s as of this writing) but it's not recognize it for some reason
    yum -y install zlib-devel curl-devel && \
    #configure does a bootstrap make under the hood
    export MAKEFLAGS=-j$(getconf _NPROCESSORS_ONLN) && \
    ./configure --system-curl && \
    make && \
    make install && \
    cd .. && \
    rm -rf cmake-3.9.0*
    
#FFmpeg build

yum install freetype-devel bzip2-devel -y && \
    mkdir ~/ffmpeg_sources
    
# Compile newer Perl and OpenSSL for ffmpeg:

curl -O -L https://www.cpan.org/src/5.0/perl-5.20.1.tar.gz && \
    tar -xf perl-5.20.1.tar.gz && \
    cd perl-5.20.1 && \
    ./Configure -des -Dprefix="$HOME/openssl_build" && \
    #perl build scripts do much redundant work
    # if running "make install" separately
    make install -j$(getconf _NPROCESSORS_ONLN) && \
    cd .. && \
    rm -rf perl-5.20.1*
    
cd ~/ffmpeg_sources && \
    curl -O -L https://github.com/openssl/openssl/archive/OpenSSL_1_1_1c.tar.gz && \
    tar -xf OpenSSL_1_1_1c.tar.gz && \
    cd openssl-OpenSSL_1_1_1c && \
    PERL="$HOME/openssl_build/bin/perl" ./config --prefix="$HOME/ffmpeg_build" --openssldir="$HOME/ffmpeg_build" shared zlib && \
    make -j$(getconf _NPROCESSORS_ONLN) && \
    #skip installing documentation
    make install_sw && \
    rm -rf ~/openssl_build
    
cd ~/ffmpeg_sources && \
    curl -O -L http://www.nasm.us/pub/nasm/releasebuilds/2.14.01/nasm-2.14.01.tar.bz2 && \
    tar -xf nasm-2.14.01.tar.bz2 && cd nasm-2.14.01 && ./autogen.sh && \
    ./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin" && \
    make -j$(getconf _NPROCESSORS_ONLN) && \
    make install
    
cd ~/ffmpeg_sources && \
    curl -O -L http://www.tortall.net/projects/yasm/releases/yasm-1.3.0.tar.gz && \
    tar -xf yasm-1.3.0.tar.gz && \
    cd yasm-1.3.0 && \
    ./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin" && \
    make -j$(getconf _NPROCESSORS_ONLN) && \
    make install
    
cd ~/ffmpeg_sources && \
    git clone --depth 1 https://chromium.googlesource.com/webm/libvpx.git && \
    cd libvpx && \
    ./configure --prefix="$HOME/ffmpeg_build" --disable-examples --disable-unit-tests --enable-vp9-highbitdepth --as=yasm --enable-pic --enable-shared && \
    make -j$(getconf _NPROCESSORS_ONLN) && \
    make install
    
cd ~/ffmpeg_sources && \
    curl -O -L https://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2 && \
    tar -xf ffmpeg-snapshot.tar.bz2 && \
    cd ffmpeg && \
    PATH=~/bin:$PATH && \
    PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" ./configure --prefix="$HOME/ffmpeg_build" --extra-cflags="-I$HOME/ffmpeg_build/include" --extra-ldflags="-L$HOME/ffmpeg_build/lib" --enable-openssl --enable-libvpx --enable-shared --enable-pic --bindir="$HOME/bin" && \
    make -j$(getconf _NPROCESSORS_ONLN) && \
    make install && \
    echo "/root/ffmpeg_build/lib/" >> /etc/ld.so.conf && \
    ldconfig && \
    rm -rf ~/ffmpeg_sources
    
export PKG_CONFIG_PATH="/usr/local/lib/pkgconfig:/root/ffmpeg_build/lib/pkgconfig"
export LDFLAGS="-L/root/ffmpeg_build/lib"

curl -O https://raw.githubusercontent.com/torvalds/linux/v4.14/include/uapi/linux/videodev2.h && \
    curl -O https://raw.githubusercontent.com/torvalds/linux/v4.14/include/uapi/linux/v4l2-common.h && \
    curl -O https://raw.githubusercontent.com/torvalds/linux/v4.14/include/uapi/linux/v4l2-controls.h && \
    curl -O https://raw.githubusercontent.com/torvalds/linux/v4.14/include/linux/compiler.h && \
    mv -f videodev2.h v4l2-common.h v4l2-controls.h compiler.h /usr/include/linux

export PATH="$HOME/bin:$PATH"

#Build OpenCV
#REQUIRES NumPy BEFORE BUILD!
#can be installed via prebuilt .whl file via:
#/opt/python/<CHOSE_VERSION>/bin/pip install <numpy_file_name>.whl

cd /root/

git clone https://github.com/skvark/opencv-python && \
cd opencv-python && \
#Change python version and flags for your needs:
export CMAKE_ARGS="-DCMAKE_C_FLAGS='-O3' -DCMAKE_CXX_FLAGS='-O3' -DUSE_VFPV3=ON -DUSE_NEON=ON -DOPENCV_FORCE_3RDPARTY_BUILD=ON -DOPENCV_ENABLE_NONFREE=ON"
/opt/python/cp36-cp36m/bin/python setup.py bdist_wheel && \
cd dist && \
auditwheel repair *.whl

#Here you go!
```

### Some Suggestions and things that I haven't done because I ran out of space on SD card ¯\_(ツ)_/¯

- This script is not robust and should be turned into `Dockerfile`
- Only OpenBLAS could be tested - ATLAS still requires some tweaking (like being build with -fPIC option or both LAPACK and  ATLAS being built as shared libraries)
- Besides OpenBLAS and ATLAS with Netlib LINPACK also [BLIS library](https://github.com/flame/blis) could be used.

### Some links that could be useful for future experiments

- https://mypages.iit.edu/~asriva13/?page_id=567
- https://web.archive.org/web/20080517012340/http://math-atlas.sourceforge.net/errata.html#completelp
- http://gyra.ualg.pt/_static/ATLAS_LAPACK_CENTOS5.4.text
- https://github.com/numpy/numpy/blob/master/site.cfg.example
- https://docs.scipy.org/doc/numpy/user/building.html
- https://proteusmaster.urcf.drexel.edu/urcfwiki/index.php/Compiling_LAPACK
- https://gist.github.com/markus-beuckelmann/8bc25531b11158431a5b09a45abd6276
