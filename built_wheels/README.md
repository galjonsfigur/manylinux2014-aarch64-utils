### Quick note

Wheels in this catalog have been built under `manylinux2014` docker image
for Python 3.6 and arm64 on Odroid N2.

If `pip` tells you that the wheel file is incompatible just change name of 
the wheel from `manylinux_2014` to `linux` - this workaround should be needed
only for older `pip` versions as `manylinux2014` is now a PEP 599 standard. 
